from django.db import models

class Categoria (models.Model):
    nombre = models.CharField(max_length=50)

class Gasto(models.Model):
    fecha = models.DateField()
    categoria = models.ForeignKey('Categoria', on_delete=models.CASCADE)
    monto = models.FloatField()
    descripcion = models.TextField(max_length=100)

