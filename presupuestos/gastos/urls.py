from django.urls import path, include
from gastos import views
from gastos.views import categoria_list,categoria_detail,gasto_list,gasto_detail

urlpatterns = [
    path('categorias/', views.categoria_list),
    path('categorias/<int:id>/', views.categoria_detail),
    path('gastos/', views.gasto_list),
    path('gastos/<int:id>/', views.gasto_detail)
]

