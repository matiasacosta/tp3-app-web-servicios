from gastos.models import Categoria, Gasto
from django.contrib.auth.models import User, Group
from rest_framework import serializers

class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = ('id', 'nombre')

class GastoSerializer(serializers.ModelSerializer):
    
    """categoria = CategoriaSerializer(required=False)
    
    def get_validation_exclusions(self):
        exclusions = super(GastoSerializer, self).get_validation_exclusions()
        return exclusions + ['categoria']"""
    class Meta:
        model = Gasto
        fields = ('id', 'fecha','monto','categoria','descripcion')
    