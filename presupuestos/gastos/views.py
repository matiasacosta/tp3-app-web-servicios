from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from gastos.models import Categoria, Gasto
from gastos.serializers import CategoriaSerializer, GastoSerializer


def categoria_list(request):
    """
    Lista todas las categorias o crea una nueva.
    """
    if request.method == 'GET':
        categorias = Categoria.objects.all()
        serializer = CategoriaSerializer(categorias, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CategoriaSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


def categoria_detail(request, id):
    """
    Devuelve, actualiza o borra una categoria.
    """
    try:
        categoria = Categoria.objects.get(id=id)
    except Categoria.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = CategoriaSerializer(categoria)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = CategoriaSerializer(categoria, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        categoria.delete()
        return HttpResponse(status=204)

@csrf_exempt
def gasto_list(request):
    """
    Lista todos los gastos o crea un nuevo gasto.
    """
    if request.method == 'GET':
        gastos = Gasto.objects.all()
        serializer = GastoSerializer(gastos, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = GastoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400) 


@csrf_exempt
def gasto_detail(request, id):
    """
    Devuelve, actualiza o borra un gasto.
    """
    try:
        gasto = Gasto.objects.get(id=id)
    except Gasto.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = GastoSerializer(gasto)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = GastoSerializer(Gasto, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        gasto.delete()
        return HttpResponse(status=204)