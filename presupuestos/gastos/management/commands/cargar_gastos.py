from django.core.management.base import BaseCommand
from datetime import timedelta, datetime, time
from gastos.models import Categoria, Gasto
import random


def rango_de_fechas(fecha_inicio, fecha_fin):
    for n in range(int ((fecha_fin - fecha_inicio).days)):
        yield fecha_inicio + timedelta(days=n)


def es_laborable(dia):
    return dia.weekday() in [1,2,3,4,5,6]


def get_categoria_random():
    categoria = Categoria.objects.all()
    indice = random.randint(0, len(categoria)-1)
    return categoria[indice]


def get_descripcion_random():
    descripciones = ['pago', 'arreglo', 'mejora', 'inversion']
    indice = random.randint(0, len(descripciones)-1)
    return descripciones[indice]


def generar_gasto(dia):
    gasto = Gasto()
    gasto.fecha = dia
    gasto.categoria = get_categoria_random()
    gasto.monto = random.uniform(100000, 1000)
    gasto.descripcion = str(get_descripcion_random())
    gasto.save()
    

class Command(BaseCommand):
    def handle(self, *args, **options):        
        fecha_inicio = datetime(2017, 1, 1)
        fecha_fin = datetime(2018, 1, 1)
        cargados = []
        for dia_actual in rango_de_fechas(fecha_inicio, fecha_fin):
            i = 0
            if es_laborable(dia_actual):
                while i < 5:
                    generar_gasto(dia_actual)
                    i += 1
