<?php
use App\Aspecto;
use App\Categoria;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*
Route::get('aspectos', function() {
    return Aspecto::all();
});
 
Route::get('aspectos/{id}', function($id) {
    return Aspecto::find($id);
});

Route::post('aspectos', function(Request $request) {
    return Aspecto::create($request->all);
});

Route::put('aspectos/{id}', function(Request $request, $id) {
    $aspecto = Aspecto::findOrFail($id);
    $aspecto->update($request->all());

    return $aspecto;
});

Route::delete('aspectos/{id}', function($id) {
    Aspecto::find($id)->delete();

    return 204;
});
*/
Route::resource('aspectos', 'AspectoController');

Route::resource('categorias', 'CategoriaController');

Route::resource('denunciantes', 'DenuncianteController');