<?php

namespace App\Http\Controllers;

use App\Aspecto;
use App\Categoria;
use App\Denunciante;
use App\EstadoObjeto;
use App\Evento;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AspectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Aspecto::all();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aspecto = new Aspecto;
        //Declaramos el nombre con el nombre enviado en el request
        $aspecto->fecha = $request->fecha;
        $aspecto->descripcion = $request->descripcion;
        $aspecto->latitud = $request->latitud;
        $aspecto->longitud = $request->longitud;
        $aspecto->categoria_id = $request->categoria_id;
        $aspecto->denunciante_id = $request->denunciante_id;
        $aspecto->fecha_ocurrencia = $request->fecha_ocurrencia;
        $aspecto->tipo = $request->tipo;
    
        //Guardamos el cambio en nuestro modelo
        $aspecto->save();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Aspecto::where('id', $id)->get();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $aspecto = Aspecto::find($id);
        $aspecto->fecha = $data['fecha'];
        $aspecto->descripcion = $data['descripcion'];
        $aspecto->latitud = $data['latitud'];
        $aspecto->longitud = $data['longitud'];
        $aspecto->categoria_id = $data['categoria_id'];
        $aspecto->denunciante_id = $data['denunciante_id'];
        $aspecto->fecha_ocurrencia = $data['fecha_ocurrencia'];
        $aspecto->tipo = $data['tipo'];
        $aspecto->save();
        return $aspecto;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aspecto = Aspecto::find($id);
        $aspecto->delete();
        return response()->json(['status' => Response::HTTP_OK]);
    }
}