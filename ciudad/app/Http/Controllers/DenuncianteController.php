<?php

namespace App\Http\Controllers;

use App\Denunciante;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DenuncianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Denunciante::all();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $denunciante = new Denunciante;
        $denunciante->telefono = $request->telefono;
        $denunciante->save();
        return response()->json(['status' => Response::HTTP_CREATED]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Denunciante::where('id', $id)->get();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $denunciante = Denunciante::find($id);
        $denunciante->telefono = $data['telefono'];
        $denunciante->save();
        return $denunciante;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $denunciante = Denunciante::find($id);
        $denunciante->delete();
        return response()->json(['status' => Response::HTTP_OK]);
    }
}