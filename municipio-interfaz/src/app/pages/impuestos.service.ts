import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ImpuestosService {

  httpOptions: any;
  
    constructor(private http: HttpClient) { 
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
        })
      };
    }
    
      get() {
        return this.http.get('http://192.168.2.75:8083/impuestos') 
          .toPromise();
      }
      
      get_tipos_impuesto() {
        return this.http.get('http://192.168.2.75:8083/tiposimpuesto') 
          .toPromise();
      }

      post(impuesto: any){
        return this.http.post('http://192.168.2.75:8083/impuestos/', impuesto,this.httpOptions) 
        .toPromise();
      }
}
