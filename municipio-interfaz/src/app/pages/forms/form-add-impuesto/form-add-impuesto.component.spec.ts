import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddImpuestoComponent } from './form-add-impuesto.component';

describe('FormAddImpuestoComponent', () => {
  let component: FormAddImpuestoComponent;
  let fixture: ComponentFixture<FormAddImpuestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAddImpuestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddImpuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
