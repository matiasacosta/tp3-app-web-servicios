import { Component, OnInit } from '@angular/core';
import { ImpuestosService } from '../../impuestos.service'

@Component({
  selector: 'form-add-impuesto',
  templateUrl: './form-add-impuesto.component.html',
  styleUrls: ['./form-add-impuesto.component.scss']
})
export class FormAddImpuestoComponent{

  tipos_impuesto: any;
  
    constructor(private impuestosService: ImpuestosService){
      this.impuestosService.get_tipos_impuesto().then(cat => this.tipos_impuesto = cat);
    }
  
    public submitted = false;
    public active = true;
   
  
    public impuesto = {'id':0, 'fecha':0,'tipo':0,'monto':'','descripcion':''};
   
    onSubmit() { this.submitted = true; 
      console.log(JSON.stringify(this.impuesto));
      this.impuestosService.post(JSON.stringify(this.impuesto)).then(respuesta => console.log(respuesta));
    }
    
    limpiar() {
      this.impuesto = {'id':0, 'fecha':0,'tipo':0,'monto':'','descripcion':''};
      this.active = false;
      setTimeout(() => this.active = true, 0);
    }

}
