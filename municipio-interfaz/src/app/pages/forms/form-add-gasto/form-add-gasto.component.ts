import { Component, OnInit } from '@angular/core';
import { PresupuestosService } from '../../presupuestos.service'

@Component({
  selector: 'form-add-gasto',
  templateUrl: './form-add-gasto.component.html',
  styleUrls: ['./form-add-gasto.component.scss']
})
export class FormAddGastoComponent {

  categorias: any;

  constructor(private gastosService: PresupuestosService){
    this.gastosService.get_categorias().then(cat => this.categorias = cat);
  }

  public submitted = false;
  public active = true;
 

  public gasto = {'fecha':'', 'categoria':0,'monto':'','descripcion':''};
 
  onSubmit() { this.submitted = true; 
    console.log(JSON.stringify(this.gasto));
    this.gastosService.post(JSON.stringify(this.gasto)).then(respuesta => console.log(respuesta));
  }
  
  limpiar() {
    this.gasto = {'fecha':'', 'categoria':0,'monto':'','descripcion':''};
    this.active = false;
    setTimeout(() => this.active = true, 0);
  }
}
