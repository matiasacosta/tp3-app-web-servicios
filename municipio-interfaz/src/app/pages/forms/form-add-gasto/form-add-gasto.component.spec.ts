import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddGastoComponent } from './form-add-gasto.component';

describe('FormAddGastoComponent', () => {
  let component: FormAddGastoComponent;
  let fixture: ComponentFixture<FormAddGastoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAddGastoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddGastoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
