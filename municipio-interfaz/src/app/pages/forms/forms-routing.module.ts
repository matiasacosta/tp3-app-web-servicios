import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormsComponent } from './forms.component';
import { FormAddAspectoComponent } from './form-add-aspecto/form-add-aspecto.component';
import { FormAddGastoComponent } from './form-add-gasto/form-add-gasto.component';
import { FormAddImpuestoComponent } from './form-add-impuesto/form-add-impuesto.component';

const routes: Routes = [{
  path: '',
  component: FormsComponent,
  children: [{
    path: 'aspecto',
    component: FormAddAspectoComponent,
  },{
    path: 'gasto',
    component: FormAddGastoComponent,
  },{
    path: 'impuesto',
    component: FormAddImpuestoComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class FormsRoutingModule {

}

export const routedComponents = [
  FormsComponent,
  FormAddAspectoComponent,
  FormAddGastoComponent,
  FormAddImpuestoComponent
];
