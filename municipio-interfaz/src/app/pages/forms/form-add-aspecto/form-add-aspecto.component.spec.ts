import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddAspectoComponent } from './form-add-aspecto.component';

describe('FormAddAspectoComponent', () => {
  let component: FormAddAspectoComponent;
  let fixture: ComponentFixture<FormAddAspectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAddAspectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddAspectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
