import { Component, OnInit } from '@angular/core';
import { CiudadService } from '../../ciudad.service'
@Component({
  selector: 'form-add-aspecto',
  templateUrl: './form-add-aspecto.component.html',
  styleUrls: ['./form-add-aspecto.component.scss']
})
export class FormAddAspectoComponent {

  categorias : any; 
  denunciantes : any; 

  constructor(private ciudadService: CiudadService){
    this.ciudadService.get_denunciantes().then(den => this.denunciantes = den);
    this.ciudadService.get_categorias().then(cat => this.categorias = cat);
  }
  
    public submitted = false;
    public active = true;
    public tipos = [{"tipo": "evento","nombre":"Evento"},{"tipo": "estadoobjecto","nombre":"Estado Objeto"}];

    public aspecto = {"fecha":"","descripcion":"","latitud":0,"longitud":0,"categoria_id":0,"denunciante_id":0,"fecha_ocurrencia":"","tipo":""};
   
    onSubmit() { this.submitted = true; 
      this.aspecto.categoria_id = Number(this.aspecto.categoria_id);
      this.aspecto.denunciante_id = Number(this.aspecto.denunciante_id);
      console.log(JSON.stringify(this.aspecto));
      this.ciudadService.post(JSON.stringify(this.aspecto)).then(respuesta => console.log(respuesta));
    }
    
    limpiar() {
      this.aspecto = {"fecha":"","descripcion":"","latitud":0,"longitud":0,"categoria_id":0,"denunciante_id":0,"fecha_ocurrencia":"","tipo":""};
      this.active = false;
      setTimeout(() => this.active = true, 0);
    }

}
