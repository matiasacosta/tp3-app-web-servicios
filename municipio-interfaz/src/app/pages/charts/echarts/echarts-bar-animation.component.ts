import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { ImpuestosService } from '../../impuestos.service';

@Component({
  selector: 'ngx-echarts-bar-animation',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsBarAnimationComponent implements AfterViewInit, OnDestroy {
  options: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService, private impuestosService: ImpuestosService) {
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors = config.variables;
      const echarts: any = config.variables.echarts;
      
      function func_filtrado(datos){
        var impuesto_inmobiliario = 0;
        var impuesto_circulacion = 0;
        var impuesto_automotor = 0;
        var impuesto_ganancia = 0;
        var impuesto_ABL = 0;
        var impuesto_lancha_aeronave = 0;
        var impuesto_pavimento_cloacas = 0;
        var impuesto_agua = 0;
        var impuesto_gas = 0;
        var impuesto_osos = 0;
        var x;
        for (x in datos) {
          if (datos[x].tipo == "5b2e37fcc568106a7a6a8eb2") {
              impuesto_inmobiliario += datos[x].monto;
          }
          else if (datos[x].tipo == "5b2e37fcc568106a7a6a8eb3") {
              impuesto_circulacion += datos[x].monto;
          }
          else if (datos[x].tipo == "5b2e37fcc568106a7a6a8eb4") {
            impuesto_automotor += datos[x].monto;
          }
          else if (datos[x].tipo == "5b2e37fcc568106a7a6a8eb5") {
            impuesto_ganancia += datos[x].monto;
          }
          else if (datos[x].tipo == "5b2e37fcc568106a7a6a8eb6") {
            impuesto_ABL += datos[x].monto;
          }
          else if (datos[x].tipo == "5b2e37fcc568106a7a6a8eb7") {
            impuesto_lancha_aeronave += datos[x].monto;
          }
          else if (datos[x].tipo == "5b2e37fcc568106a7a6a8eb8") {
            impuesto_pavimento_cloacas += datos[x].monto;
          } 
          else if (datos[x].tipo == "5b2e37fcc568106a7a6a8eb9") {
            impuesto_agua += datos[x].monto;
          }
          else if (datos[x].tipo == "5b2e37fcc568106a7a6a8eba") {
            impuesto_gas += datos[x].monto;
          }
          else {
              impuesto_osos += datos[x].monto;
          }
        }
        return {
             impuesto_inmobiliario: impuesto_inmobiliario, 
             impuesto_circulacion: impuesto_circulacion,  
             impuesto_automotor: impuesto_automotor, 
             impuesto_ganancia: impuesto_ganancia, 
             impuesto_ABL: impuesto_ABL,
             impuesto_lancha_aeronave: impuesto_lancha_aeronave, 
             impuesto_pavimento_cloacas: impuesto_pavimento_cloacas, 
             impuesto_agua: impuesto_agua, 
             impuesto_gas: impuesto_gas,
             impuesto_osos: impuesto_osos 
        };  

      }

      this.impuestosService.get().then(
        datos => {
          var datos_filtrados = func_filtrado(datos);
          this.options = {
          backgroundColor: echarts.bg,
          color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight,
                  colors.primaryLight],
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)',
          },
          legend: {
            orient: 'vertical',
            left: 'left',
            data: ['Impuesto Inmobiliario', 'Impuesto de Circulacion', 'Impuesto Automotor',
                   'Impuesto a la Ganancia', 'Impuesto ABL', 'Impuesto a Lanchas y Aeronaves',
                   'Impuesto de Pavimento y Cloacas', 'Impuesto de Agua Potable',
                   'Impuesto de Gas', 'Impuesto de Patrulla Anti Osos'],
            textStyle: {
              color: echarts.textColor,
            },
          },
          series: [
            {
              name: 'Gastos por Categoria',
              type: 'pie',
              radius: '40%',
              center: ['50%', '75%'],
              data: [
                { value: datos_filtrados.impuesto_inmobiliario, name: 'Impuesto Inmobiliario' },
                { value: datos_filtrados.impuesto_circulacion, name: 'Impuesto de Circulacion' },
                { value: datos_filtrados.impuesto_automotor, name: 'Impuesto Automotor' },
                { value: datos_filtrados.impuesto_ganancia, name: 'Impuesto a la Ganancia' },
                { value: datos_filtrados.impuesto_ABL, name: 'Alumbrado/Vacheo/Etc.' },
                { value: datos_filtrados.impuesto_lancha_aeronave, name: 'Impuesto a Lanchas y Aeronaves' },
                { value: datos_filtrados.impuesto_pavimento_cloacas, name: 'Impuesto de Pavimento y Cloacas' },
                { value: datos_filtrados.impuesto_agua, name: 'Impuesto de Agua Potable' },
                { value: datos_filtrados.impuesto_gas, name: 'Impuesto de Gas' },
                { value: datos_filtrados.impuesto_osos, name: 'Impuesto de Patrulla Anti Osos' },
              ],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: echarts.itemHoverShadowColor,
                },
              },
              label: {
                normal: {
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
              },
              labelLine: {
                normal: {
                  lineStyle: {
                    color: echarts.axisLineColor,
                  },
                },
              },
            },
          ],
        }
      }
      );  
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}