import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { PresupuestosService } from '../../presupuestos.service';


@Component({
  selector: 'ngx-echarts-pie',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsPieComponent implements AfterViewInit, OnDestroy {
  options: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService,private gastosService: PresupuestosService) {
  }

  
  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors = config.variables;
      const echarts: any = config.variables.echarts;
      
      function func_filtrado(datos){
        var gastos_sueldos = 0;
        var gastos_talleres = 0;
        var gastos_deportes = 0;
        var gastos_limpieza = 0;
        var gastos_alumbrado_bacheo_etc = 0;
        var gastos_arte_cultura = 0;
        var gastos_seguridad = 0;
        var gastos_salud = 0;
        var gastos_educacion = 0;
        var gastos_otros = 0;
        var x;
        for (x in datos) {
          if (datos[x].categoria == 1) {
              gastos_sueldos += datos[x].monto;
          }
          else if (datos[x].categoria == 2) {
              gastos_talleres += datos[x].monto;
          }
          else if (datos[x].categoria == 3) {
            gastos_deportes += datos[x].monto;
          }
          else if (datos[x].categoria == 4) {
            gastos_limpieza += datos[x].monto;
          }
          else if (datos[x].categoria == 5) {
            gastos_alumbrado_bacheo_etc += datos[x].monto;
          }
          else if (datos[x].categoria == 6) {
            gastos_arte_cultura += datos[x].monto;
          }
          else if (datos[x].categoria == 7) {
            gastos_seguridad += datos[x].monto;
          } 
          else if (datos[x].categoria == 8) {
            gastos_salud += datos[x].monto;
          }
          else if (datos[x].categoria == 9) {
            gastos_educacion += datos[x].monto;
          }
          else {
              gastos_otros += datos[x].monto;
          }
        }
        return {
             gastos_sueldos: gastos_sueldos, 
             gastos_talleres: gastos_talleres,  
             gastos_deportes: gastos_deportes, 
             gastos_limpieza: gastos_limpieza, 
             gastos_alumbrado_bacheo_etc: gastos_alumbrado_bacheo_etc,
             gastos_arte_cultura: gastos_arte_cultura, 
             gastos_seguridad: gastos_seguridad, 
             gastos_salud: gastos_salud, 
             gastos_educacion: gastos_educacion,
             gastos_otros: gastos_otros 
        };  

      }

      this.gastosService.get().then(
        datos => {
          var datos_filtrados = func_filtrado(datos);
          this.options = {
          backgroundColor: echarts.bg,
          color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight,
                  colors.primaryLight],
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)',
          },
          legend: {
            orient: 'vertical',
            left: 'left',
            data: ['Sueldos Empleados', 'Talleres', 'Deportes', 'Limpieza', 'Alumbrado/Vacheo/Etc.',
                   'Arte y Cultura', 'Seguridad', 'Salud', 'Educacion', 'Otros'],
            textStyle: {
              color: echarts.textColor,
            },
          },
          series: [
            {
              name: 'Gastos por Categoria',
              type: 'pie',
              radius: '50%',
              center: ['50%', '70%'],
              data: [
                { value: datos_filtrados.gastos_sueldos, name: 'Sueldos Empleados' },
                { value: datos_filtrados.gastos_talleres, name: 'Talleres' },
                { value: datos_filtrados.gastos_deportes, name: 'Deportes' },
                { value: datos_filtrados.gastos_limpieza, name: 'Limpieza' },
                { value: datos_filtrados.gastos_alumbrado_bacheo_etc, name: 'Alumbrado/Vacheo/Etc.' },
                { value: datos_filtrados.gastos_arte_cultura, name: 'Arte y Cultura' },
                { value: datos_filtrados.gastos_seguridad, name: 'Seguridad' },
                { value: datos_filtrados.gastos_salud, name: 'Salud' },
                { value: datos_filtrados.gastos_educacion, name: 'Educacion' },
                { value: datos_filtrados.gastos_otros, name: 'Otros' },
              ],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: echarts.itemHoverShadowColor,
                },
              },
              label: {
                normal: {
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
              },
              labelLine: {
                normal: {
                  lineStyle: {
                    color: echarts.axisLineColor,
                  },
                },
              },
            },
          ],
        }
      }
      );  
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
