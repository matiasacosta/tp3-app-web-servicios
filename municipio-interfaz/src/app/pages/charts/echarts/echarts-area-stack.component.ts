import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { CiudadService } from '../../ciudad.service';

@Component({
  selector: 'ngx-echarts-area-stack',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsAreaStackComponent implements AfterViewInit, OnDestroy {
  options: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService, private aspectosService: CiudadService) {
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;
    
      function func_filtrado(datos){
        var accidente_transito = 0;
        var pelea = 0;
        var robo = 0;
        var animal_suelto = 0;
        var otros_eventos = 0;
        var vereda_rota = 0;
        var pared_pintada = 0;
        var sem_roto = 0;
        var perdida_agua = 0;
        var otros_estados = 0;
        var x;
        for (x in datos) {
          if (datos[x].categoria_id == 1) {
              accidente_transito += 1;
          }
          if (datos[x].categoria_id == 2) {
              pelea += 1;
          }
          if (datos[x].categoria_id == 3) {
            robo += 1;
          }
          if (datos[x].categoria_id == 4) {
            animal_suelto += 1;
          }
          if (datos[x].categoria_id == 5) {
            otros_eventos += 1;
          }
          if (datos[x].categoria_id == 6) {
            vereda_rota += 1;
          }
          if (datos[x].categoria_id == 7) {
            pared_pintada += 1;
          } 
          if (datos[x].categoria_id == 8) {
            sem_roto += 1;
          }
          if (datos[x].categoria_id == 9) {
            perdida_agua += 1;
          }
          else {
              otros_estados += 1;
          }
        }
        return {
             accidente_transito: accidente_transito, 
             pelea: pelea,  
             robo: robo, 
             animal_suelto: animal_suelto, 
             otros_eventos: otros_eventos,
             vereda_rota: vereda_rota, 
             pared_pintada: pared_pintada, 
             sem_roto: sem_roto, 
             perdida_agua: perdida_agua,
             otros_estados: otros_estados 
        };  

      }

    this.aspectosService.get().then(
        datos => {
          var datos_filtrados = func_filtrado(datos);
      this.options = {
        backgroundColor: echarts.bg,
        color: [colors.danger, colors.warning],
        tooltip: {},
        legend: {
          data: ['Aspectos'],
          textStyle: {
            color: echarts.textColor,
          },
        },
        radar: {
          name: {
            textStyle: {
              color: echarts.textColor,
            },
          },
          indicator: [
            { name: 'Pelea', max: 3 },
            { name: 'Accidente de Transito', max: 9 },
            { name: 'Vereda Rota', max: 3 },
            { name: 'Pared Pintada', max: 3 },
            { name: 'Otros Eventos', max: 2 },
            { name: 'Otros Estados', max: 15 },
          ],
          splitArea: {
            areaStyle: {
              color: 'transparent',
            },
          },
        },
        series: [
          {
            name: 'Aspectos',
            type: 'radar',
            data: [
              {
                value: [datos_filtrados.pelea,
                        datos_filtrados.accidente_transito,
                        datos_filtrados.vereda_rota,
                        datos_filtrados.pared_pintada,
                        datos_filtrados.otros_eventos,
                        datos_filtrados.otros_estados],
                name: 'Aspectos',
              }
            ],
          },
        ],
      };
    }
    );
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
