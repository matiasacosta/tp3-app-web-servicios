import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { PresupuestosService } from '../../presupuestos.service';

@Component({
  selector: 'ngx-echarts-bar',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsBarComponent implements AfterViewInit, OnDestroy {
  options: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService, private gastosService: PresupuestosService) {
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      function func_filtrado(datos){
        var enero = 0;
        var febrero = 0;
        var marzo = 0;
        var abril = 0;
        var mayo = 0;
        var junio = 0;
        var julio = 0;
        var agosto = 0;
        var septiembre = 0;
        var octubre = 0;
        var noviembre = 0;
        var diciembre = 0;
        var x;
        for (x in datos) {
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-01-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-02-01').getTime()) ){
              enero += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-02-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-03-01').getTime()) ){
              febrero += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-03-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-04-01').getTime()) ){
            marzo += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-04-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-05-01').getTime()) ){
            abril += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-05-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-06-01').getTime()) ){
            mayo += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-06-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-07-01').getTime()) ){
            junio += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-07-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-08-01').getTime()) ){
            julio += datos[x].monto;
          } 
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-08-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-09-01').getTime()) ){
            agosto += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-09-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-10-01').getTime()) ){
            septiembre += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-10-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-11-01').getTime()) ){
            octubre += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-11-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-12-01').getTime()) ){
            noviembre += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-12-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2018-01-01').getTime()) ){
            diciembre += datos[x].monto;
          }
        }
        return {
             enero: enero, 
             febrero: febrero,  
             marzo: marzo, 
             abril: abril, 
             mayo: mayo,
             junio: junio, 
             julio: julio, 
             agosto: agosto, 
             septiembre: septiembre,
             octubre: octubre,
             noviembre: noviembre,
             diciembre: diciembre 
        };  

      }

      this.gastosService.get().then(
        datos => {
          var datos_filtrados = func_filtrado(datos);
          this.options = {
            backgroundColor: echarts.bg,
            color: [colors.primaryLight],
            tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow',
            },
            },
            grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true,
            },
            xAxis: [
            {
                type: 'category',
                data: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
                        'Septiembre', 'Octubbre', 'Noviembre', 'Diciembre'],
                axisTick: {
                alignWithLabel: true,
                },
                axisLine: {
                lineStyle: {
                    color: echarts.axisLineColor,
                },
                },
                axisLabel: {
                textStyle: {
                    color: echarts.textColor,
                },
                },
            },
            ],
            yAxis: [
            {
                type: 'value',
                axisLine: {
                lineStyle: {
                    color: echarts.axisLineColor,
                },
                },
                splitLine: {
                lineStyle: {
                    color: echarts.splitLineColor,
                },
                },
                axisLabel: {
                textStyle: {
                    color: echarts.textColor,
                },
                },
            },
            ],
            series: [
            {
                name: 'Pesos',
                type: 'bar',
                barWidth: '60%',
                data: [datos_filtrados.enero, datos_filtrados.febrero, datos_filtrados.marzo, datos_filtrados.abril,
                       datos_filtrados.mayo, datos_filtrados.junio, datos_filtrados.julio, datos_filtrados.agosto,
                       datos_filtrados.septiembre, datos_filtrados.octubre, datos_filtrados.noviembre,
                       datos_filtrados.diciembre ],
            },
            ],
        };
      }
      );  
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
