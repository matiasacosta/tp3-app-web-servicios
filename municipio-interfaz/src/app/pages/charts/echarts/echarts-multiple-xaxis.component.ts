import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { PresupuestosService } from '../../presupuestos.service';
import { ImpuestosService } from '../../impuestos.service';

@Component({
  selector: 'ngx-echarts-multiple-xaxis',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsMultipleXaxisComponent implements AfterViewInit, OnDestroy {
  options: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService, private presupuestoService: PresupuestosService,
              private impuestoService: ImpuestosService) {
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      function func_filtrado(datos){
        var enero = 0;
        var febrero = 0;
        var marzo = 0;
        var abril = 0;
        var mayo = 0;
        var junio = 0;
        var julio = 0;
        var agosto = 0;
        var septiembre = 0;
        var octubre = 0;
        var noviembre = 0;
        var diciembre = 0;
        var x;
        for (x in datos) {
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-01-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-02-01').getTime()) ){
              enero += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-02-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-03-01').getTime()) ){
              febrero += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-03-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-04-01').getTime()) ){
            marzo += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-04-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-05-01').getTime()) ){
            abril += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-05-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-06-01').getTime()) ){
            mayo += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-06-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-07-01').getTime()) ){
            junio += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-07-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-08-01').getTime()) ){
            julio += datos[x].monto;
          } 
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-08-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-09-01').getTime()) ){
            agosto += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-09-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-10-01').getTime()) ){
            septiembre += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-10-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-11-01').getTime()) ){
            octubre += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-11-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2017-12-01').getTime()) ){
            noviembre += datos[x].monto;
          }
          if( (new Date(datos[x].fecha).getTime() >= new Date('2017-12-01').getTime()) && 
              (new Date(datos[x].fecha).getTime() < new Date('2018-01-01').getTime()) ){
            diciembre += datos[x].monto;
          }
        }
        return {
             enero: enero, 
             febrero: febrero,  
             marzo: marzo, 
             abril: abril, 
             mayo: mayo,
             junio: junio, 
             julio: julio, 
             agosto: agosto, 
             septiembre: septiembre,
             octubre: octubre,
             noviembre: noviembre,
             diciembre: diciembre 
        };  


      }

      this.presupuestoService.get().then(
          presupuesto => { this.impuestoService.get().then(
            impuesto => {
              var presupuesto_filtrados = func_filtrado(presupuesto);
              var impuesto_filtrados = func_filtrado(impuesto);
              this.options = {
                backgroundColor: echarts.bg,
                color: [colors.success, colors.info],
                tooltip: {
                  trigger: 'none',
                  axisPointer: {
                    type: 'cross',
                  },
                },
                legend: {
                  data: ['2017 Gastos Realizados', '2017 Recaudacion Impuestos'],
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
                grid: {
                  top: 70,
                  bottom: 50,
                },
                xAxis: [
                  {
                    type: 'category',
                    axisTick: {
                      alignWithLabel: true,
                    },
                    axisLine: {
                      onZero: false,
                      lineStyle: {
                        color: colors.info,
                      },
                    },
                    axisLabel: {
                      textStyle: {
                        color: echarts.textColor,
                      },
                    },
                    axisPointer: {
                      label: {
                        formatter: params => {
                          return (
                            'Impuestos  ' + params.value + (params.seriesData.length ? '：' + params.seriesData[0].data : '')
                          );
                        },
                      },
                    },
                    data: [
                      '2017-1',
                      '2017-2',
                      '2017-3',
                      '2017-4',
                      '2017-5',
                      '2017-6',
                      '2017-7',
                      '2017-8',
                      '2017-9',
                      '2017-10',
                      '2017-11',
                      '2017-12',
                    ],
                  },
                  {
                    type: 'category',
                    axisTick: {
                      alignWithLabel: true,
                    },
                    axisLine: {
                      onZero: false,
                      lineStyle: {
                        color: colors.success,
                      },
                    },
                    axisLabel: {
                      textStyle: {
                        color: echarts.textColor,
                      },
                    },
                    axisPointer: {
                      label: {
                        formatter: params => {
                          return (
                            'Gastos  ' + params.value + (params.seriesData.length ? '：' + params.seriesData[0].data : '')
                          );
                        },
                      },
                    },
                    data: [
                      '2017-1',
                      '2017-2',
                      '2017-3',
                      '2017-4',
                      '2017-5',
                      '2017-6',
                      '2017-7',
                      '2017-8',
                      '2017-9',
                      '2017-10',
                      '2017-11',
                      '2017-12',
                    ],
                  },
                ],
                yAxis: [
                  {
                    type: 'value',
                    axisLine: {
                      lineStyle: {
                        color: echarts.axisLineColor,
                      },
                    },
                    splitLine: {
                      lineStyle: {
                        color: echarts.splitLineColor,
                      },
                    },
                    axisLabel: {
                      textStyle: {
                        color: echarts.textColor,
                      },
                    },
                  },
                ],
                series: [
                  {
                    name: '2017 Gastos Realizados',
                    type: 'line',
                    xAxisIndex: 1,
                    smooth: true,
                    data: [presupuesto_filtrados.enero, presupuesto_filtrados.febrero, presupuesto_filtrados.marzo,
                           presupuesto_filtrados.abril, presupuesto_filtrados.mayo, presupuesto_filtrados.junio,
                           presupuesto_filtrados.julio, presupuesto_filtrados.agosto, presupuesto_filtrados.septiembre,
                           presupuesto_filtrados.octubre, presupuesto_filtrados.noviembre, presupuesto_filtrados.diciembre],
                  },
                  {
                    name: '2017 Recaudacion Impuestos',
                    type: 'line',
                    smooth: true,
                    data: [impuesto_filtrados.enero, impuesto_filtrados.febrero, impuesto_filtrados.marzo,
                      impuesto_filtrados.abril, impuesto_filtrados.mayo, impuesto_filtrados.junio,
                      impuesto_filtrados.julio, impuesto_filtrados.agosto, impuesto_filtrados.septiembre,
                      impuesto_filtrados.octubre, impuesto_filtrados.noviembre, impuesto_filtrados.diciembre],
                  },
                ],
              }
            }
          )
        }
      );
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
