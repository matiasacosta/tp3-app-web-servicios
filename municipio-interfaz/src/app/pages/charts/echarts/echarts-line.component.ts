import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { CiudadService } from '../../ciudad.service';

@Component({
  selector: 'ngx-echarts-line',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsLineComponent implements AfterViewInit, OnDestroy {
  options: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService, private aspectosService: CiudadService) {
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors = config.variables;
      const echarts: any = config.variables.echarts;
      
      function func_filtrado(datos){
        var accidente_transito = 0;
        var pelea = 0;
        var robo = 0;
        var animal_suelto = 0;
        var otros_eventos = 0;
        var vereda_rota = 0;
        var pared_pintada = 0;
        var sem_roto = 0;
        var perdida_agua = 0;
        var otros_estados = 0;
        var x;
        for (x in datos) {
          if (datos[x].categoria_id == 1) {
              accidente_transito += 1;
          }
          else if (datos[x].categoria_id == 2) {
              pelea += 1;
          }
          else if (datos[x].categoria_id == 3) {
            robo += 1;
          }
          else if (datos[x].categoria_id == 4) {
            animal_suelto += 1;
          }
          else if (datos[x].categoria_id == 5) {
            otros_eventos += 1;
          }
          else if (datos[x].categoria_id == 6) {
            vereda_rota += 1;
          }
          else if (datos[x].categoria_id == 7) {
            pared_pintada += 1;
          } 
          else if (datos[x].categoria_id == 8) {
            sem_roto += 1;
          }
          else if (datos[x].categoria_id == 9) {
            perdida_agua += 1;
          }
          else {
              otros_estados += 1;
          }
        }
        return {
             accidente_transito: accidente_transito, 
             pelea: pelea,  
             robo: robo, 
             animal_suelto: animal_suelto, 
             otros_eventos: otros_eventos,
             vereda_rota: vereda_rota, 
             pared_pintada: pared_pintada, 
             sem_roto: sem_roto, 
             perdida_agua: perdida_agua,
             otros_estados: otros_estados 
        };  

      }

      this.aspectosService.get().then(
        datos => {
          var datos_filtrados = func_filtrado(datos);
          this.options = {
          backgroundColor: echarts.bg,
          color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight,
                  colors.primaryLight],
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)',
          },
          legend: {
            orient: 'vertical',
            left: 'left',
            data: ['Accidente de Transito', 'Pelea', 'Robo', 'Animal Suelto', 'Otros Eventos',
                   'Vereda Rota', 'Pared Pintada', 'Semaforo sin Funcionamiento',
                   'Perdida de Agua', 'Otros Estados'],
            textStyle: {
              color: echarts.textColor,
            },
          },
          series: [
            {
              name: 'Gastos por Categoria',
              type: 'pie',
              radius: '39%',
              center: ['50%', '70%'],
              data: [
                { value: datos_filtrados.pelea, name: 'Pelea' },
                { value: datos_filtrados.robo, name: 'Robo' },
                { value: datos_filtrados.animal_suelto, name: 'Animal Suelto' },
                { value: datos_filtrados.otros_eventos, name: 'Otros Eventos' },
                { value: datos_filtrados.vereda_rota, name: 'Vereda Rota' },
                { value: datos_filtrados.pared_pintada, name: 'Pared Pintada' },
                { value: datos_filtrados.sem_roto, name: 'Semaforo sin Funcionamiento' },
                { value: datos_filtrados.perdida_agua, name: 'Perdida de Agua' },
                { value: datos_filtrados.otros_estados, name: 'Otros Estados' },
                { value: datos_filtrados.accidente_transito, name: 'Accidente de Transito' },
              ],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: echarts.itemHoverShadowColor,
                },
              },
              label: {
                normal: {
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
              },
              labelLine: {
                normal: {
                  lineStyle: {
                    color: echarts.axisLineColor,
                  },
                },
              },
            },
          ],
        }
      }
      );  
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
