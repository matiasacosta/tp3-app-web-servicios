import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PresupuestosService {

  httpOptions: any;

  constructor(private http: HttpClient) { 
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
  }
  
    get() {
      return this.http.get('http://192.168.2.75:8081/gastos/') 
        .toPromise();
    }
    
    get_categorias(){
      return this.http.get('http://192.168.2.75:8081/categorias/') 
      .toPromise();
    }

    post(gasto: any){
      return this.http.post('http://192.168.2.75:8081/gastos/',gasto,this.httpOptions) 
      .toPromise();
    }
}
