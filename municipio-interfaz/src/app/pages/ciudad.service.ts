import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CiudadService {

  httpOptions: any;

  constructor(private http: HttpClient) { 
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
  }
  
    get() {
      return this.http.get('http://192.168.2.75:8082/api/aspectos/') 
        .toPromise();
    }
    
    get_categorias() {
      return this.http.get('http://192.168.2.75:8082/api/categorias/') 
        .toPromise();
    }

    get_denunciantes() {
      return this.http.get('http://192.168.2.75:8082/api/denunciantes/') 
        .toPromise();
    }

    post(aspectos: any){
      return this.http.post('http://192.168.2.75:8082/api/aspectos/',aspectos,this.httpOptions) 
      .toPromise();
    }
}
