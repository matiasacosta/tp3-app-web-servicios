import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Graficos',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Dashboard',
        link: '/pages/charts/echarts',
      }
    ],
  },
  {
    title: 'Altas a los servidores',
    icon: 'nb-arrow-thin-up',
    children: [
      {
        title: 'Cargar un Gasto',
        link: '/pages/forms/gasto',
      },
      {
        title: 'Cargar un Aspecto',
        link: '/pages/forms/aspecto',
      },
      {
        title: 'Cargar un Impuesto',
        link: '/pages/forms/impuesto',
      },
    ],
  },
];
