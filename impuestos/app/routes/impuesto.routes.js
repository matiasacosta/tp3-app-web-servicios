module.exports = (app) => {
    const impuestos = require('../controllers/impuesto.controller.js');

    // Crea un nuevo impuesto
    app.post('/impuestos', impuestos.create);

    // Devuelve todos los impuestos
    app.get('/impuestos', impuestos.findAll);

    // Devuelve un solo impuesto usando su impuestoId
    app.get('/impuestos/:impuestoId', impuestos.findOne);

    // Edita un impuesto con impuestoId
    app.put('/impuestos/:impuestoId', impuestos.update);

    // Borra un impuesto con impuestoId
    app.delete('/impuestos/:impuestoId', impuestos.delete);

    // Carga datos en la base
    app.get('/impuestos/base/carga', impuestos.cargarImpuestos);
}