module.exports = (app) => {
    const tipoimpuesto = require('../controllers/tipoimpuesto.controller.js');

    // Crea un nuevo Tipo de Impuesto
    app.post('/tiposimpuesto', tipoimpuesto.create);

    // Devuelve todos los Tipos de Impuesto
    app.get('/tiposimpuesto', tipoimpuesto.findAll);

    // Devuelve un solo Tipo de Impuesto usando su tipoimpuestoId
    app.get('/tiposimpuesto/:tipoimpuestoId', tipoimpuesto.findOne);

    // Borra un tipo de impuesto usando tipoimpuestoId
    app.put('/tiposimpuesto/:tipoimpuestoId', tipoimpuesto.update);

    // Borra un tipo de impuesto usando tipoimpuestoId
    app.delete('/tiposimpuesto/:tipoimpuestoId', tipoimpuesto.delete);

    // Carga datos en la base
    app.get('/tiposimpuesto/base/carga', tipoimpuesto.cargarTiposImpuesto);
}