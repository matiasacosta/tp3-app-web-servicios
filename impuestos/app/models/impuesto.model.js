const mongoose = require('mongoose');

const ImpuestoSchema = mongoose.Schema({
    id: Number,
    fecha: { type: Date, default: Date.now },
    tipo: {type: mongoose.Schema.Types.ObjectId, ref: 'TipoImpuesto'},
    monto: Number,
    descripcion: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Impuesto', ImpuestoSchema);
