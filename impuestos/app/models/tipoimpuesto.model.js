const mongoose = require('mongoose');

const TipoImpuestoSchema = mongoose.Schema({
    id: Number,
    nombre: String
}, {
    timestamps: true
});

module.exports = mongoose.model('TipoImpuesto', TipoImpuestoSchema);