const Impuesto = require('../models/impuesto.model.js');
const TipoImpuesto = require('../models/tipoimpuesto.model.js');

// Crear y Guardar un nuevo Impuesto
exports.create = (req, res) => {
    // Validate request
    if(!req.body) {
        return res.status(400).send({
            message: "El impuesto no puede ser vacio"
        });
    }
    
    TipoImpuesto.findOne({ id: req.body.tipo}, function (err, doc){
         // Crear un impuesto
        const impuesto = new Impuesto({
            id: req.body.id,
            fecha: req.body.fecha,
            tipo: doc._id,
            monto: req.body.monto,
            descripcion: req.body.descripcion
        });

        // Guardar un Impuesto en la Base de Datos
        impuesto.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocurrió un error mientras se creaba el impuesto."
            });
        });
    });
};

// Buscar y devolver todos los impuestos de la base de datos.
exports.findAll = (req, res) => {
    Impuesto.find()
    .then(impuestos => {
        res.send(impuestos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Ocurrió un error cuando se buscaban los impuestos."
        });
    });
};

// Encuenta un impuesto con su impuestoId
exports.findOne = (req, res) => {
    Impuesto.findOne({id:req.params.impuestoId})
    .then(impuesto => {
        if(!impuesto.id) {
            return res.status(404).send({
                message: "No se encontró un impuestio con el id: " + req.params.impuestoId
            });            
        }
        res.send(impuesto);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "No se encontró un impuesto con el id: " + req.params.impuestoId
            });                
        }
        return res.status(500).send({
            message: "Ocurrió un error mientras se buscaba el impuesto con el id: " + req.params.impuestoId
        });
    });
};

// Modificar un impuesto utilizando su impuestoId
exports.update = (req, res) => {
    // Validate Request
    if(!req.body) {
        return res.status(400).send({
            message: "El contenido del impuesto no puede estar vacio"
        });
    }

    // Encuentra un impuesto con su impuestoId y lo actualiza
    Impuesto.findOneAndUpdate({id:req.params.impuestoId}, {
        id: req.body.id,
        fecha: req.body.fecha,
        tipo: req.body.tipo,
        monto: req.body.monto,
        descripcion: req.body.descripcion
    }, {new: true})
    .then(impuesto => {
        if(!impuesto) {
            return res.status(404).send({
                message: "No se encontró el impuesto con el id: " + req.params.impuestoId
            });
        }
        res.send(impuesto);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "No se encontró el impuesto con el id: " + req.params.impuestoId
            });                
        }
        return res.status(500).send({
            message: "Ocurrió un error actualizando el impuesto con el id: " + req.params.impuestoId
        });
    });
};

// Borra unn impuesto dado su impuestoId
exports.delete = (req, res) => {
    Impuesto.findOneAndDelete({id:req.params.impuestoId})
    .then(impuesto => {
        if(!impuesto) {
            return res.status(404).send({
                message: "No se encontró el impuesto con el id: " + req.params.impuestoId
            });
        }
        res.send({message: "Impuesto borrado correctamente!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "No se encontró el impuesto con el id: " + req.params.impuestoId
            });                
        }
        return res.status(500).send({
            message: "No se pudo borrar el impuesto con el id: " + req.params.impuestoId
        });
    });
};

function get_descripcion(nombre,mes){
    var meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
    'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    
    return nombre + ' ' + meses[mes] + ' 2017';

}

function get_fecha(mes){

    if (mes > 9){
        return "2017-" + mes + "-01";
    }else{
        return "2017-0" + mes + "-01";
    }
}


// Cargamos la base con impuestos
exports.cargarImpuestos =  (req,res) => {
    TipoImpuesto.find().then(tiposimpuesto => {
        var count = 0;
        for (i=0;i<12;i++){
            for (j=0;j<400;j++){
                for (k=0;k<10;k++){
                    count++;
                    var impuesto = new Impuesto({
                        id: count,
                        fecha: get_fecha(i+1),
                        tipo: tiposimpuesto[k]._id,
                        monto: Math.floor(Math.random() * 2000) + 500,
                        descripcion: get_descripcion(tiposimpuesto[k].nombre,i)
                    });
                    impuesto.save();
                    console.log(count);
                };
            };
        };
        res.send('Base de datos cargada!');
    });
};