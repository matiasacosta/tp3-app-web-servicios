const TipoImpuesto = require('../models/tipoimpuesto.model.js');

// Crear y Guardar un nuevo Tipo de Impuesto
exports.create = (req, res) => {
    // Validate request
    if(!req.body) {
        return res.status(400).send({
            message: "El Tipo de Impuesto no puede ser vacio"
        });
    }

    // Crear un Tipo de Impuesto
    const tipoimpuesto = new TipoImpuesto({
        id: req.body.id,
        nombre: req.body.nombre
    });

    // Guardar un Tipo de Impuesto en la Base de Datos
    tipoimpuesto.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Ocurrió un error mientras se creaba el tipo de impuesto."
        });
    });
};

// Buscar y devolver todos los tipos de impuesto de la base de datos.
exports.findAll = (req, res) => {
    TipoImpuesto.find()
    .then(tiposimpuesto => {
        res.send(tiposimpuesto);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Ocurrió un error cuando se buscaban los Tipos de Impuesto."
        });
    });
};

// Encuenta un tipo de impuesto con su tipoimpuestoId
exports.findOne = (req, res) => {
    TipoImpuesto.findOne({id:req.params.tipoimpuestoId})
    .then(tipoimpuesto => {
        if(!tipoimpuesto.id) {
            return res.status(404).send({
                message: "No se encontró un Tipo de Impuesto con el id: " + req.params.tipoimpuestoId
            });            
        }
        res.send(tipoimpuesto);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "No se encontró un tipo de impuesto con el id: " + req.params.tipoimpuestoId
            });                
        }
        return res.status(500).send({
            message: "Ocurrió un error mientras se buscaba el tipo de impuesto con el id: " + req.params.tipoimpuestoId
        });
    });
};

// Modificar un Tipo de Impuesto utilizando su tipoimpuestoId
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "El contenido del impuesto no puede estar vacio"
        });
    }

    // Encuentra un tipo de impuesto con su tipoimpuestoId y lo actualiza
    Impuesto.findOneAndUpdate({id:req.params.tipoimpuestoId}, {
        id: req.body.id,
        nombre: req.body.nombre
    }, {new: true})
    .then(tipoimpuesto => {
        if(!tipoimpuesto) {
            return res.status(404).send({
                message: "No se encontró el tipo de impuesto con el id: " + req.params.tipoimpuestoId
            });
        }
        res.send(tipoimpuesto);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "No se encontró el tipo de impuesto con el id: " + req.params.tipoimpuestoId
            });                
        }
        return res.status(500).send({
            message: "Ocurrió un error actualizando el tipo de impuesto con el id: " + req.params.tipoimpuestoId
        });
    });
};

// Borra unn impuesto dado su impuestoId
exports.delete = (req, res) => {
    TipoImpuesto.findOneAndDelete({id:req.params.tipoimpuestoId})
    .then(tipoimpuesto => {
        if(!tipoimpuesto) {
            return res.status(404).send({
                message: "No se encontró el tipo de impuesto con el id: " + req.params.tipoimpuestoId
            });
        }
        res.send({message: "Tipo de Impuesto borrado correctamente!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "No se encontró el tipo de impuesto con el id: " + req.params.tipoimpuestoId
            });                
        }
        return res.status(500).send({
            message: "No se pudo borrar el tipo de impuesto con el id: " + req.params.tipoimpuestoId
        });
    });
};

// Cargamos la base con tipos de impuestos
exports.cargarTiposImpuesto =  (req,res) => {
    // create some events
    const tiposImpuesto = [
      { id: 1, nombre: 'Impuesto Inmobiliario' },
      { id: 2, nombre: 'Impuesto a la Circulacion' },
      { id: 3, nombre: 'Impuesto Automotor' },
      { id: 4, nombre: 'Impuesto a las Ganancias' },
      { id: 5, nombre: 'Impuesto de ABL' },
      { id: 6, nombre: 'Impuesto sobre lanchas y aeronaves' },
      { id: 7, nombre: 'Tasa de pavimentos y cloacas' },
      { id: 8, nombre: 'Impuesto por tendido de red de agua potable'},
      { id: 9, nombre: 'Impuesto por extensión de la red de gas'},
      { id: 10, nombre: 'Impuesto de la patrulla anti-osos'}  
    ];
  
    // use the Event model to insert/save
    for (tipo of tiposImpuesto) {
      var nuevoTipo = new TipoImpuesto(tipo);
      nuevoTipo.save();
    };
  
    // seeded!
    res.send('Base de datos cargada!');
};