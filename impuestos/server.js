const express = require('express');
const bodyParser = require('body-parser');


const app = express();


app.use(bodyParser.urlencoded({ extended: true }))


app.use(bodyParser.json())


const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;


mongoose.connect(dbConfig.url)
.then(() => {
    console.log("Conexion a la base exitosa");    
}).catch(err => {
    console.log('Ocurrió un problema al conectarse a la base');
    process.exit();
});


app.get('/', (req, res) => {
    res.json({"Mensaje": "Bienvenido a la aplicación municipal de Impuestos."});
});

require('./app/routes/tipoimpuesto.routes.js')(app);
require('./app/routes/impuesto.routes.js')(app);

app.listen(8083, '0.0.0.0' ,() => {
    console.log("Servidor escuchando en el puerto 8083");
});